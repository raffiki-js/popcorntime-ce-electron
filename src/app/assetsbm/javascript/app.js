const logger = require('electron-timber');

MyApp = new Backbone.Marionette.Application();

MyApp.addRegions({
  content: "#content"
});

MyApp.vent.on("layout:rendered", function(){
  logger.log("layout rendered");
});
