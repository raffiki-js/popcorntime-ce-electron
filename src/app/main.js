//require('update-electron-app')({
//  logger: require('electron-log')
//})

// Modules to control application life and create native browser window
const electron = require('electron');
// Module to control application life.
const app = electron.app;
const ipcMain = electron.ipcMain;
//const ses = electron.session;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');
const glob = require('glob')
const autoUpdater = require('electron-updater').autoUpdater;
const nativeImage = electron.nativeImage;
const logger = require('electron-timber');

const debug = /--debug/.test(process.argv[2])

if (process.mas) app.setName('PopcornTime-ce')

let mainWindow = null

let ico = nativeImage.createFromPath(path.join(__dirname, '/images/icon.png'))

function initializeMain () {
  makeSingleInstance()
  loadPrefiles();
// Create mainWindow, load the rest of the app, etc...
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

  function createWindow () {
    // Create the browser window. with customized options
    const winOptions = {
      minWidth: 860,
      minHeight: 480,
      Width: 960,
      Height: 540,
      darkTheme: true,
      show: false,
      autoHideMenuBar: true,
      icon: ico,
      title: app.getName(),
        webPreferences: {
          nodeIntegration: true
        }
    }

    mainWindow = new BrowserWindow(winOptions);

    // and load the index.html of the app.
    mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file:',
      slashes: true
    }))

    // Launch fullscreen with DevTools open, usage: npm run debug
    if (debug) {
      mainWindow.webContents.openDevTools()
      mainWindow.maximize()
      require('devtron').install()
    }
    // Show when ready without flickering
    mainWindow.once('ready-to-show', () => {
      mainWindow.show()
      logger.log('Showing the browser');
    })

    //mainWindow.webContents.send('must-close', 'clear-first')
    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
      mainWindow = null
    })
  }

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
// On macOS it is common for applications and their menu bar
// to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

// get app settings
// When receiving a quitAndInstall signal, quit and install the new version ;)
//ipcMain.on("", (event, arg) => {
//  autoUpdater.quitAndInstall();
//});
}


function makeSingleInstance () {
  if (process.mas) return

  var gotTheLock = app.requestSingleInstanceLock()

  if (!gotTheLock) {
    //if app is locked then Quit
    app.quit()
  } else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
      // Someone tried to run a second instance, we should focus our window.
      if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore()
        mainWindow.focus()
      }
    })
  }
}

// Make this app a single instance app.
//
// The main window will be restored and focused instead of a second window
// opened when a person attempts to launch a second instance.
//
// Returns true if the current version of the app should quit instead of
// launching.
function makeSingleInstance () {
  if (process.mas) return

  app.requestSingleInstanceLock()

  app.on('second-instance', () => {
    if (mainWindow) {
      if (mainWindow.isMinimized()) mainWindow.restore()
      mainWindow.focus()
    }
  })
}

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
function loadPrefiles () {
  const xtfiles = glob.sync(path.join(__dirname, 'mainpre/*.js'))
  xtfiles.forEach((xtfile) => { require(xtfile) })
}

initializeMain()
