const electron = require('electron');
const {app, ipcMain, dialog} = require('electron')
const logger = require('electron-timber');

let tray = null

app.on('will-finish-launching', () => {

  ipcMain.on('get-app-path', (event) => {
    event.returnValue = app.getAppPath()
  })

  ipcMain.on('get-data-path', (event) => {
    event.returnValue = app.getPath('userData')
  })

})
